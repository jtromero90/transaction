import { ApiProperty } from "@nestjs/swagger"

export class CreateTransactionDto {

    transactionExternalId?: number
    @ApiProperty()
    accountExternalIdDebit: number
    @ApiProperty()
    accountExternalIdCredit: number
    @ApiProperty()
    tranferTypeId: number
    @ApiProperty()
    value: number
    createdAt?: Date
    statusid?: number

}
