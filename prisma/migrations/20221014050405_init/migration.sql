/*
  Warnings:

  - You are about to drop the `article` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE `article`;

-- CreateTable
CREATE TABLE `Transaction` (
    `transactionExternalId` INTEGER NOT NULL AUTO_INCREMENT,
    `accountExternalIdDebit` INTEGER NOT NULL,
    `accountExternalIdCredit` INTEGER NOT NULL,
    `tranferTypeId` INTEGER NOT NULL,
    `value` DOUBLE NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `statusid` INTEGER NOT NULL,

    PRIMARY KEY (`transactionExternalId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
